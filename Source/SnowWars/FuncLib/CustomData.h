// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"

#include "CustomData.generated.h"

UENUM(BlueprintType)
enum class EGamePhase : uint8
{
    NormalPhase UMETA(DisplayName = "Normal Phase"),
    HardPhase UMETA(DisplayName = "Hard Phase"),
    FinalPhase UMETA(DisplayName = "Final Phase")
};

UENUM(BlueprintType)
enum class ESpawnerType : uint8
{
    SnowdriftSpawner UMETA(DisplayName = "Snowdrift Spawner"),
    EnemySpawner UMETA(DisplayName = "Enemy Spawner"),
    None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class ESpawnLocationType : uint8
{
   AreaSpawn UMETA(DisplayName = "Area Spawn"),
   PointSpawn UMETA(DisplayName = "Point Spawn")
};

UCLASS()
class SNOWWARS_API UCustomData : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
};