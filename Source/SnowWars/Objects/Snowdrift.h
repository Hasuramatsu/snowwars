// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Interactable.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Snowdrift.generated.h"

UCLASS()
class SNOWWARS_API ASnowdrift : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnowdrift();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Storage")
	int32 MaxSnowballValue = 3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundBase* PickupSound = nullptr;
private:
	//Root scene Component
	UPROPERTY(Category = Component, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	USceneComponent* SceneComponent = nullptr;

	//Mesh component
	UPROPERTY(Category = Component, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMesh = nullptr;

	//Collision Sphere
	UPROPERTY(Category = Component, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	USphereComponent* CollisionSphere = nullptr;

	int32 CurrentSnowballValue = 0;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Interact with Actor
	virtual void Interact(AActor* Interactor) override;

	UFUNCTION()
	void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
};
