// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Interactable.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Icecream.generated.h"

UCLASS()
class SNOWWARS_API AIcecream : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIcecream();

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	int32 HealValue = 30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float LifeTime = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float RotationSpeed = 1.f;
	

	//sound played on pickup
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundBase* PickupSound = nullptr;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"), Category = Components)
	USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMesh = nullptr;

	//RotationTimer
	FTimerHandle RotatorTimerHandler;
	float RotateTime = 0.05f;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Heal if player
	virtual void Interact(AActor* Interactor) override;

	//use interact on overlap player
	UFUNCTION()
	void CollisionSphereOverlap(UPrimitiveComponent* OverlappedComponent,
        AActor* OtherActor,
        UPrimitiveComponent* OtherComp,
        int32 OtherBodyIndex,
        bool bFromSweep,
        const FHitResult& SweepResult);

	//Rotate by Yaw for RotateSpeed each call
	UFUNCTION()
	void Rotate();
};


