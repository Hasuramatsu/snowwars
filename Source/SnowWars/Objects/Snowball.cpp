// Fill out your copyright notice in the Description page of Project Settings.


#include "Snowball.h"



#include "Kismet/GameplayStatics.h"
#include "SnowWars/Pawn/Enemy_Base.h"
#include "SnowWars/Pawn/Pawn_Base.h"


// Sets default values
ASnowball::ASnowball()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create root collision sphere
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetSphereRadius(20.f);
	CollisionSphere->SetCollisionProfileName("OverlapAll");
	CollisionSphere->SetSimulatePhysics(true);
	RootComponent = CollisionSphere;

	//Create mesh
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionProfileName("NoCollision");
	StaticMesh->SetupAttachment(RootComponent);

	//Create projectile movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjextileMovement"));
	ProjectileMovement->UpdatedComponent = RootComponent;

	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->bRotationFollowsVelocity = true;

	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ASnowball::CollisionSphereBeginOverlap);
	CollisionSphere->OnComponentHit.AddDynamic(this, &ASnowball::CollisionSphereHit);
}

// Called when the game starts or when spawned
void ASnowball::BeginPlay()
{
	Super::BeginPlay();

	if(SpawnSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), SpawnSound, GetActorLocation(), FRotator::ZeroRotator);
	}
}

// Called every frame
void ASnowball::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnowball::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//Check actor is pawn
	APawn_Base* MyPawn = Cast<APawn_Base>(OtherActor);
	if(MyPawn)
	{
		//KILL HIM
		MyPawn->BecomeDead();
	}
	this->Destroy();
}

void ASnowball::CollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	AEnemy_Base* HitEnemy = Cast<AEnemy_Base>(OtherActor);
	if(HitEnemy)
	{
		//KILL HIM
		HitEnemy->Defeat();
	}
	if(HitSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation(), FRotator::ZeroRotator);
	}
	this->Destroy();
}

