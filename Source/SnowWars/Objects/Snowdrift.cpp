// Fill out your copyright notice in the Description page of Project Settings.


#include "Snowdrift.h"


#include "Kismet/GameplayStatics.h"
#include "SnowWars/Game/SnowWarsGameModeBase.h"
#include "SnowWars/Pawn/Pawn_Base.h"

// Sets default values
ASnowdrift::ASnowdrift()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(RootComponent);

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetSphereRadius(52.f);
	CollisionSphere->SetupAttachment(RootComponent);
	CollisionSphere->SetCollisionProfileName("OverlapOnlyPawn");
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ASnowdrift::CollisionSphereBeginOverlap);

	CurrentSnowballValue = FMath::RandRange(1, MaxSnowballValue);
}

// Called when the game starts or when spawned
void ASnowdrift::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ASnowdrift::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnowdrift::Interact(AActor* Interactor)
{
	// Check interacted actor is pawn
	APawn_Base* MyPawn = Cast<APawn_Base>(Interactor);
	if(MyPawn)
	{
		//Add snowballs
		MyPawn->PickupSnowball(CurrentSnowballValue);
		UGameplayStatics::PlaySound2D(GetWorld(), PickupSound);
		//and destroy this actor
		this->Destroy();
	}
}

void ASnowdrift::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Interact(OtherActor);
}

