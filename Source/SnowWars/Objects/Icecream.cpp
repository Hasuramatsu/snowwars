// Fill out your copyright notice in the Description page of Project Settings.


#include "Icecream.h"


#include "Kismet/GameplayStatics.h"
#include "SnowWars/Pawn/Pawn_Base.h"

// Sets default values
AIcecream::AIcecream()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Setup collision sphere
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->SetSphereRadius(50.f);
	CollisionSphere->SetSimulatePhysics(false);
	RootComponent = CollisionSphere;

	//Setup mesh
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetCollisionProfileName("NoCollision");
	StaticMesh->SetupAttachment(RootComponent);

	//Bind overlap func
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AIcecream::CollisionSphereOverlap);

}

// Called when the game starts or when spawned
void AIcecream::BeginPlay()
{
	Super::BeginPlay();

	//Set life time of actor
	SetLifeSpan(LifeTime);

	GetWorld()->GetTimerManager().SetTimer(RotatorTimerHandler, this, &AIcecream::Rotate, RotateTime, true);
}

// Called every frame
void AIcecream::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AIcecream::Interact(AActor* Interactor)
{
	APawn_Base* MyPawn = Cast<APawn_Base>(Interactor);
	if(MyPawn)
	{
		MyPawn->HealHealth(HealValue);
	}
	UGameplayStatics::PlaySound2D(GetWorld(), PickupSound);
	this->Destroy();
}

void AIcecream::CollisionSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	APawn_Base* MyPawn = Cast<APawn_Base>(OtherActor);
	if(MyPawn)
	{
		Interact(OtherActor);
	}
}

void AIcecream::Rotate()
{
	AddActorWorldRotation(FRotator(0.f, RotationSpeed, 0.f));
}
