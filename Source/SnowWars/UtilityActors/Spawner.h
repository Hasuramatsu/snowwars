// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "NavigationSystem.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "SnowWars/FuncLib/CustomData.h"

#include "Spawner.generated.h"

UCLASS()
class SNOWWARS_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	//Class for spawn
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnParameters")
	TSubclassOf<AActor> SpawnedActor;

	//Spawn switch
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnParameters")
	bool bSpawnerActive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnParameters")
	ESpawnLocationType SpawnType = ESpawnLocationType::AreaSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnParameters")
	float RangeToPlayerForSpawn = 300.f;
	
private:
	//Default component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", Category = Components))
	USceneComponent* SceneComponent = nullptr;

	//Spawn area
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", Category = Components))
	UBoxComponent* BoxComponent = nullptr;

	//Type 
	UPROPERTY(VisibleAnywhere, Category = "Info")
	ESpawnerType Type = ESpawnerType::None;
	
	//Center of spawn radius
	FVector CurrentLocation;
	//Location for actor spawn
	FVector SpawnLocation;	

	//Time handler for spawn timer
	FTimerHandle SpawnerTimerHandle;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Spawn actor
	UFUNCTION(BlueprintCallable)
	bool SpawnNewActor(bool Forced = false);

	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ESpawnerType GetType() const {return Type;}
};
