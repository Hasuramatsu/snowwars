// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"


#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "SnowWars/Game/SnowWarsGameModeBase.h"
#include "SnowWars/Objects/Snowdrift.h"
#include "SnowWars/Pawn/Enemy_Base.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Setup root component
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	//Setup spawn area box
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnArea"));
	BoxComponent->SetCollisionProfileName("NoCollision");
	BoxComponent->SetGenerateOverlapEvents(false);
	BoxComponent->SetupAttachment(RootComponent);

	
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
	//GetSpawnPoint
	CurrentLocation = GetActorLocation();
	SpawnLocation = CurrentLocation;
	//Set type of spawner
	if(SpawnedActor->IsChildOf(ASnowdrift::StaticClass()))
	{
		Type = ESpawnerType::SnowdriftSpawner;
	}
	else if(SpawnedActor->IsChildOf(AEnemy_Base::StaticClass()))
	{
		Type = ESpawnerType::EnemySpawner;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ASpawner::ASpawner -- Undefined spawn class"));
	}	

	//Add spawner to gamemode
	ASnowWarsGameModeBase* MyGameMode = Cast<ASnowWarsGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if(MyGameMode)
	{
		MyGameMode->AddSpawner(this);
	}

	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ASpawner::SpawnNewActor(bool Forced)
{
	//Change spawn point to area
	if(SpawnType == ESpawnLocationType::AreaSpawn)
	{
		const FVector OriginLocation = BoxComponent->Bounds.Origin;
		const FVector ExtentLocation = BoxComponent->Bounds.BoxExtent;
		bool bIsOk = false;
		int32 Counter = 0;
		while(!bIsOk && UGameplayStatics::GetPlayerPawn(GetWorld(), 0))
		{	
			SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(OriginLocation, ExtentLocation);
			FVector PlayerLocation = UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetActorLocation();	
			Counter++;
			FVector RangeVector = SpawnLocation - PlayerLocation;
			if(RangeVector.Size() > RangeToPlayerForSpawn)
			{
				bIsOk = true;
			}
			if(Counter > 50)
			{
				if(Forced)
				{
					bIsOk = true;
				}
				else
				{
					return false;
				}
				
			}
		}
		
		SpawnLocation.Z = CurrentLocation.Z;
	}
	const FRotator Rotation = GetActorRotation();
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	//Spawn actor
	ASnowWarsGameModeBase* MyGameMode = Cast<ASnowWarsGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	if(MyGameMode)
	{
		MyGameMode->TrackSpawnedActor(GetWorld()->SpawnActor(SpawnedActor, &SpawnLocation, &Rotation, SpawnParameters), GetType());
	}
	return true;
}

