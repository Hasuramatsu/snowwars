// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn_Base.h"


#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "SnowWars/Game/SnowWarsGameModeBase.h"
#include "SnowWars/Game/SWPlayerState.h"

// Sets default values
APawn_Base::APawn_Base()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"));
    CapsuleComponent->InitCapsuleSize(42.f, 96.f);
    RootComponent = CapsuleComponent;
    CapsuleComponent->SetCollisionProfileName(TEXT("Pawn"));

    Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh"));
    Mesh->SetupAttachment(RootComponent);

    //Create camera boom
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true);
    CameraBoom->TargetArmLength = 800.f;
    CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false;
    CameraBoom->bEnableCameraLag = true;
    CameraBoom->CameraLagSpeed = 3.f;

    //Create main camera
    MainCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
    MainCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    MainCamera->bUsePawnControlRotation = false;

    //Create movement component
    MovementComponent = CreateDefaultSubobject<UPawnBaseMovementComponent>(TEXT("MovementComponent"));
    MovementComponent->UpdatedComponent = RootComponent;

}

// Called when the game starts or when spawned
void APawn_Base::BeginPlay()
{
    Super::BeginPlay();

    //Setup starting health
    CurrentHealth = MaxHealth;
    
}

// Called every frame
void APawn_Base::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    MovementTick(DeltaTime);
    FireTick();
}

// Called to bind functionality to input
void APawn_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis(TEXT("ForwardMovement"), this, &APawn_Base::InputAxisX);
    PlayerInputComponent->BindAxis(TEXT("RightMovement"), this, &APawn_Base::InputAxisY);

    PlayerInputComponent->BindAction<FSetFire>(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &APawn_Base::SetIsFiring, true);
    PlayerInputComponent->BindAction<FSetFire>(TEXT("FireEvent"), EInputEvent::IE_Released, this, &APawn_Base::SetIsFiring, false);
    PlayerInputComponent->BindAction(TEXT("DodgeEvent"), EInputEvent::IE_Pressed, this, &APawn_Base::Dodge);
}

void APawn_Base::MovementTick(float DeltaTime)
{
    if(bIsAlive)
    {
        if (MovementComponent && (MovementComponent->UpdatedComponent == RootComponent))
        {
            if(!bDodgeActivated)
            {
                MovementComponent->AddInputVector(FVector(1.f, 0.f, 0.f) * AxisX);
                MovementComponent->AddInputVector(FVector(0.f, 1.f, 0.f) * AxisY);
            }
            else if(bDodgeActivated)
            {
                MovementComponent->AddInputVector(DodgeDirection);
            }
        }
        APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
        if(MyController)
        {
            FHitResult ResultHit;
            MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
            const float Yaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
            SetActorRotation(FQuat(FRotator(0.f, Yaw, 0.f)));
        }
    }

    
}

void APawn_Base::FireTick()
{
    if(bIsFiring && bFireTimerReady && bIsAlive)
    {
        Fire();
    }
}

void APawn_Base::Fire()
{
    const bool bCanFire = bFireTimerReady && CurrentAmmo > 0;
    if(bCanFire)
    {
        bFireTimerReady = false;
        SpawnSnowball();
        CurrentAmmo--;
        GetWorld()->GetTimerManager().SetTimer(FireTimerHandle, this, &APawn_Base::ResetFireTimer, FireCooldown);
    }
}

void APawn_Base::ResetFireTimer()
{
    bFireTimerReady = true;
    GetWorld()->GetTimerManager().ClearTimer(FireTimerHandle);
}

void APawn_Base::SpawnSnowball()
{
    FActorSpawnParameters SpawnParams;
    SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
    SpawnParams.Owner = this;
    SpawnParams.Instigator = this;
    const FVector Location = GetActorLocation() + GetActorForwardVector() * 50;
    const FRotator Rotation = GetActorRotation() + SnowballAngle;
    GetWorld()->SpawnActor<ASnowball>(SnowballClass, Location, Rotation, SpawnParams);
}

void APawn_Base::SetIsFiring(bool Value)
{
    bIsFiring = Value;
}

void APawn_Base::ChangeHealthByValue(int32 Value)
{
    CurrentHealth += Value;
    if(CurrentHealth > MaxHealth)
    {
        CurrentHealth = MaxHealth;
    }
    else if(CurrentHealth < 0)
    {
        CurrentHealth = 0;
    }
    if(CurrentHealth == 0 && bIsAlive)
    {
        BecomeDead();
    }
}

void APawn_Base::DamageHealth(int32 Value)
{
    ChangeHealthByValue(-Value);
}

void APawn_Base::HealHealth(int32 Value)
{
    ChangeHealthByValue(Value);
}

void APawn_Base::PickupSnowball(int32 Amount)
{
    OnAmmoPickup.Broadcast(Amount);
    CurrentAmmo += Amount;
}

float APawn_Base::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
    AActor* DamageCauser)
{
    const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
    DamageHealth(ActualDamage);
    return ActualDamage;
}

void APawn_Base::BecomeDead()
{
    bIsAlive = false;
    OnDead.Broadcast();
    //GetPlayerState
    ASWPlayerState* MyPlayerState = GetController()->GetPlayerState<ASWPlayerState>();
    if(MyPlayerState)
    {
        if(MyPlayerState->GetCurrentLives() <= 1)
        {
            EndGame();
        }
        else
        {
            APlayerController* MyController = Cast<APlayerController>(GetController());
            if(MyController)
            {
                MyController->bAutoManageActiveCameraTarget = false;
                MyController->UnPossess();
                SetLifeSpan(LifeSpanDuration);
            }
        }
    }
    else
    {
        EndGame();
    }
}

void APawn_Base::EndGame() const
{
    ASnowWarsGameModeBase* MyGameMode = Cast<ASnowWarsGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
    if(MyGameMode)
    {
        MyGameMode->EndGame();
    }  
}

void APawn_Base::Dodge()
{
    if(bDodgeReady)
    {
        DodgeTimer = DodgeCooldown;
        CapsuleComponent->SetCollisionProfileName("DodgePawn");
        bDodgeReady = false;
        bDodgeActivated = true;
        MovementComponent->CurrentMaxSpeed = DodgeSpeed;
        DodgeDirection = GetLastMovementInputVector();
        GetWorld()->GetTimerManager().SetTimer(DodgeDurationTimerHandler, this, &APawn_Base::SetDodgeActivated, DodgeDuration, false);
        GetWorld()->GetTimerManager().SetTimer(DodgeCooldownTimerHandler, this, &APawn_Base::DodgeUpdate, 0.1, true);
    }
}

void APawn_Base::SetDodgeActivated()
{
    bDodgeActivated = false;
    MovementComponent->CurrentMaxSpeed = MovementComponent->NormalMaxSpeed;
}

void APawn_Base::DodgeUpdate()
{
    DodgeTimer -= 0.1;
    if(DodgeTimer <= 0)
    {
        GetWorld()->GetTimerManager().ClearTimer(DodgeCooldownTimerHandler);
        bDodgeReady = true;    
        CapsuleComponent->SetCollisionProfileName("Pawn");
    }
}
