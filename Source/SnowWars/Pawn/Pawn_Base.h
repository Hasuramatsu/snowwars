// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/PawnBaseMovementComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "SnowWars/Objects/Snowball.h"


#include "Pawn_Base.generated.h"

DECLARE_DELEGATE_OneParam(FSetFire, const bool);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoPickup, int32, Amount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS()
class SNOWWARS_API APawn_Base : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawn_Base();

	// Returns collision capsule
	UCapsuleComponent* GetCapsule() const {return CapsuleComponent;}
	//Returns pawn mesh
	UStaticMeshComponent* GetMesh() const {return Mesh;}
	//Returns main camera
	UCameraComponent* GetMainCamera() const {return MainCamera;}
	//Returns camera boom
	USpringArmComponent* GetCameraBoom() const {return CameraBoom;}
	//Returns movement component
	virtual UPawnMovementComponent* GetMovementComponent() const override{return MovementComponent;};

	//Snowball class for spawn
	UPROPERTY(EditDefaultsOnly, Category = "Snowballs")
	TSubclassOf<ASnowball> SnowballClass;

	//Start angle
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Snowballs")
	FRotator SnowballAngle;

	//Shooting Cooldown
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Snowballs")
	float FireCooldown = 1.f;//Shooting cooldown

	//
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Characteristics")
	int32 MaxHealth = 100;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Characteristics")
	float LifeSpanDuration = 10.f;

	//Cant take while invulnerable
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Characteristics")
	bool bDodgeActivated = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dodge")
	float DodgeDuration = 0.5f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dodge")
	float DodgeCooldown = 5.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Dodge")
	float DodgeSpeed = 1200.f;



	
	
private:
	//Collision capsule
	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UCapsuleComponent* CapsuleComponent = nullptr;
	
	//Pawn static mesh
	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh = nullptr;

	//Main camera
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UCameraComponent* MainCamera = nullptr;

	//Camera boom
	UPROPERTY(Category = Camera, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom = nullptr;

	//Movement Component
	UPROPERTY(Category = Movement, VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UPawnBaseMovementComponent* MovementComponent = nullptr;

	//Axis values
	float AxisX = 0.f;
	float AxisY = 0.f;
	float AxisZ = 0.f;

	//Flags
	bool bIsFiring = false;
	bool bFireTimerReady = true;
	bool bIsAlive = true;
	bool bDodgeReady = true;

	//Timers
	FTimerHandle FireTimerHandle;
	FTimerHandle DodgeDurationTimerHandler;
	FTimerHandle DodgeCooldownTimerHandler;

	UPROPERTY()
	float DodgeTimer = 0.f;

	//CurrentDodgeDirection
	FVector DodgeDirection;

	//DynamicStats
	int32 CurrentAmmo = 0;
	int32 CurrentHealth = 100;

	//Delegates
	UPROPERTY(BlueprintAssignable)
	FOnAmmoPickup OnAmmoPickup;
	UPROPERTY(BlueprintAssignable)
	FOnDead OnDead;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	//Move player every frame based on input axis values
	void MovementTick(float DeltaTime);
	//
	void FireTick();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Input func
	void InputAxisX(float Value){AxisX = Value;};
	void InputAxisY(float Value){AxisY = Value;};	

	//Fire func

	//Logic of fire snowball
	UFUNCTION()
	void Fire();
	//Reset timer for fire
	void ResetFireTimer();
	//Spawn snowball logic
	void SpawnSnowball();

	//Setters
	UFUNCTION()
	void SetIsFiring(bool Value);

	//Getters
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentAmmo() const {return CurrentAmmo;}
	UFUNCTION(BlueprintCallable)
    int32 GetCurrentHealth() const {return CurrentHealth;}
	UFUNCTION(BlueprintCallable)
    int32 GetMaxHealth() const {return MaxHealth;}
	UFUNCTION(BlueprintCallable)
	bool IsAlive() const {return bIsAlive;};
	UFUNCTION(BlueprintCallable)
	float GetDodgeTimer() const {return DodgeTimer;}


	//Health Func
	void ChangeHealthByValue(int32 Value); //Increase current by value
	void DamageHealth(int32 Value);	//Use Change health by negative value
	void HealHealth(int32 Value); //Use Change health by positive value

	
	//Increase wielded snowball by value
	UFUNCTION(BlueprintCallable)
	void PickupSnowball(int32 Amount);

	//Delegate func
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	
	//Start Death
	UFUNCTION(BlueprintCallable)
	void BecomeDead();

	//Call GameMode endgame
	void EndGame() const;

	//Use dodge
	UFUNCTION(BlueprintCallable)
	void Dodge();
	//DodgeCooldown
	void DodgeUpdate();

	//Set bDodgeActivated to false;
	UFUNCTION(BlueprintCallable)
    void SetDodgeActivated();
	//return DodgeActivated
	UFUNCTION(BlueprintCallable)
	bool IsDodgeActivated() const {return bDodgeActivated;}


	//TODO
	UFUNCTION(BlueprintCallable)
	FVector GetDodgeDirection() const {return DodgeDirection;}
};


