// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/PawnBaseAttackComponent.h"
#include "GameFramework/Character.h"
#include "SnowWars/FuncLib/PawnInterface.h"
#include "SnowWars/Objects/Icecream.h"


#include "Enemy_Base.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDefeat, AActor*, Self);

UCLASS()
class SNOWWARS_API AEnemy_Base : public ACharacter, public IPawnInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy_Base();
	
	//Delegates
	UPROPERTY(BlueprintCallable)
	FOnDefeat OnDefeat;
private:
	//TimeHandlers
	FTimerHandle AttackTimeHandler;
	//Flags
	bool bIsAlive = true;
	bool bCanMove = true;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Component)
	UPawnBaseAttackComponent* AttackComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Drop")
	TSubclassOf<AIcecream> Drop;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (ClampMin = "0", ClampMax = "100",UIMin = "0", UIMax = "100"), Category = Drop)
	int32 DropChance = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed")
	float WalkSpeed = 200.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Speed")
	float ChasingSpeed = WalkSpeed;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool IsAlive() override {return bIsAlive;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool CanAttack() override {return AttackComponent->CanAttack();}
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual bool CanMove() override {return bCanMove;}

	//Play on death
	UFUNCTION()
	void Defeat();

	//Called by GameMode on difficulty up
	UFUNCTION()
	void IncreaseChasingSpeedByValue(float Value);
	UFUNCTION()
	void IncreaseDamageByValue(float Value);

	//Getters
	float* GetActualChasingSpeed() {return &ChasingSpeed;}
	float* GetActualDamage() {return &AttackComponent->MeleeAttackDamage;}
};
