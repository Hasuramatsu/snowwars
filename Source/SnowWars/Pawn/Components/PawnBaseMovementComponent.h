// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "PawnBaseMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class SNOWWARS_API UPawnBaseMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

public:
	//Tick override
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float NormalMaxSpeed = 500.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Stats")
	float CurrentMaxSpeed = 500.f;
	//Return MaxSpeed
	float GetNormalMaxSpeed() const {return NormalMaxSpeed;}
	float GetCurrentMaxSpeed() const {return CurrentMaxSpeed;}
private:

};
