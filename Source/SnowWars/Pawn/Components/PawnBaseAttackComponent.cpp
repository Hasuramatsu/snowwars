// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBaseAttackComponent.h"


#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "SnowWars/Game/SnowWarsGameModeBase.h"
#include "SnowWars/Pawn/Enemy_Base.h"
#include "SnowWars/Pawn/Pawn_Base.h"


// Sets default values for this component's properties
UPawnBaseAttackComponent::UPawnBaseAttackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPawnBaseAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPawnBaseAttackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPawnBaseAttackComponent::MeleeAttack()
{
	if(bCanAttack)
	{
		bCanAttack = false;
		//Set temp data for trace
		FHitResult HitResult;
		const FVector StartLocation = GetOwner()->GetActorLocation();
		const FVector EndLocation = StartLocation + GetOwner()->GetActorForwardVector() * MeleeAttackRange;
		const ETraceTypeQuery MyTraceType = UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel2);
		TArray<AActor*> IgnoredActors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemy_Base::StaticClass(), IgnoredActors);

		//Trace for melee attack
		UKismetSystemLibrary::SphereTraceSingle(GetWorld(),
			StartLocation,
			EndLocation,
			MeleeAttackRadius,
			MyTraceType,
			true,
			IgnoredActors,
			EDrawDebugTrace::None,
			HitResult,
			true,
			FLinearColor::Green,
			FLinearColor::Red,
			3.f
			);
		//Apply Damage to actor hit
		//TODO on notify
		APawn_Base* MyPawn = Cast<APawn_Base>(HitResult.GetActor());
		
		if(MyPawn && !MyPawn->IsDodgeActivated())
		{
			if(HitSound)
			{
				HitLocation = HitResult.Location;
				GetWorld()->GetTimerManager().SetTimer(HitSoundTimerhandler, this, &UPawnBaseAttackComponent::PlayHitSound, 0.2);
			}
			UGameplayStatics::ApplyDamage(HitResult.GetActor(), MeleeAttackDamage, GetOwner()->GetInstigatorController(), GetOwner(), UDamageType::StaticClass());
		}
		//Set and start cooldown
		const float AttackTime = DelayAfterAttack + MeleeAttackAnimation->GetPlayLength();
		GetWorld()->GetTimerManager().SetTimer(AttackTimeHandler, this, &UPawnBaseAttackComponent::ResetAttack, AttackTime, false);

		ACharacter* MyCharacter = Cast<ACharacter>(GetOwner());
		if(MyCharacter)
		{
			if(MyCharacter->GetMesh()->SkeletalMesh)
			{
				MyCharacter->PlayAnimMontage(MeleeAttackAnimation);
			}
		}
	}
}

void UPawnBaseAttackComponent::ResetAttack()
{
	bCanAttack = true;
	GetWorld()->GetTimerManager().ClearTimer(AttackTimeHandler);
}

void UPawnBaseAttackComponent::PlayHitSound() const
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitLocation, FRotator::ZeroRotator);
}


