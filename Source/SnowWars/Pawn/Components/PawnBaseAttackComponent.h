// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PawnBaseAttackComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SNOWWARS_API UPawnBaseAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPawnBaseAttackComponent();

private:
	//Time handlers
	FTimerHandle AttackTimeHandler;
	FTimerHandle HitSoundTimerhandler;
	//Flags
	bool bCanAttack = true;

	//trash
	FVector HitLocation;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	float DelayAfterAttack = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	float MeleeAttackRange = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	float MeleeAttackRadius = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attack")
	float MeleeAttackDamage = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
	UAnimMontage* MeleeAttackAnimation = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* HitSound = nullptr;


	//Perform attack if can
	UFUNCTION(BlueprintCallable)
	void MeleeAttack();

	UFUNCTION(BlueprintCallable)
	void ResetAttack();

	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool CanAttack()const {return bCanAttack;}

	UFUNCTION()
	void PlayHitSound() const;
};
