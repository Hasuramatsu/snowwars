// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy_Base.h"

#include "Kismet/GameplayStatics.h"
#include "SnowWars/Game/SWPlayerState.h"

// Sets default values
AEnemy_Base::AEnemy_Base()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AttackComponent = CreateDefaultSubobject<UPawnBaseAttackComponent>(TEXT("AttackComponent"));
}


// Called when the game starts or when spawned
void AEnemy_Base::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemy_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void AEnemy_Base::Defeat()
{
	//Say it defeated
	OnDefeat.Broadcast(this);

	//Add score for player
	ASWPlayerState* MyPlayerState = UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPlayerState<ASWPlayerState>();
	if(MyPlayerState)
	{
		MyPlayerState->AddScore();
	}

	//TODO dead anim

	//Spawn icecream
	if(Drop)
	{
		int32 Random = FMath::FRandRange(0, 100);
		if(Random <= DropChance)
		{
			FVector Location = GetActorLocation();
			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			GetWorld()->SpawnActor(Drop, &Location, &FRotator::ZeroRotator, SpawnParams);
		}
	}
	//Destroy this actor
	this->Destroy();
}

void AEnemy_Base::IncreaseChasingSpeedByValue(float Value)
{
	ChasingSpeed += Value;
}

void AEnemy_Base::IncreaseDamageByValue(float Value)
{
	AttackComponent->MeleeAttackDamage += Value;
}

