// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "SnowWars/FuncLib/CustomData.h"

#include "SWGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDifficultyPointChange, int32, Points);
/**
 * 
 */
UCLASS()
class SNOWWARS_API ASWGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
    UPROPERTY(BlueprintAssignable)
    FOnDifficultyPointChange OnDifficultyPointChange;
   //Max Difficulty
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
    int32 MaxDifficulty = 5;
    //Max Difficulty Points for bar 
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
    int32 MaxDifficultyPoints = 10;
    //Start value
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
    int32 StartDifficultyPoints = -20;
    //Increased point by score divided by this
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
    int32 DivideCoef = 10;

    //Delay for spawn snowdrifts
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
    float SnowdriftSpawnTimer = 1.f;
    //Max count of snowdrift at one moment
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
    int32 MaxSnowdrifts = 15;
    //Delay for spawn enemy
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
    float EnemySpawnTimer = 1.f;
    //Max count of enemies alive
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner")
    int32 MaxEnemies = 10;
    
private:
    //Current Difficulty
    UPROPERTY(VisibleAnywhere, Category = "Game")
    int32 DifficultyLevel = 1;

    //Difficulty Points for bar 
    UPROPERTY(VisibleAnywhere, Category = "Game")
    int32 DifficultyPoints = -20;

    //Current game phase
    UPROPERTY(VisibleAnywhere, Category = "Game")
    EGamePhase GamePhase = EGamePhase::NormalPhase;

    //Update difficulty called by UpdateDifficultyPoints
    UFUNCTION()
    void UpdateDifficulty();

public:
    //set Difficulty points based on player score
    UFUNCTION()
    void UpdateDifficultyPoints(int32 Score);

    //Set current game phase
    void SetGamePhase(EGamePhase Phase);
    
    //Getters
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetDifficultyLevel() const {return DifficultyLevel;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetMaxDifficulty() const {return MaxDifficulty;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetDifficultyPoints() const {return DifficultyPoints;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetMaxDifficultyPoints() const {return MaxDifficultyPoints;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    float GetSnowdriftSpawnTimer() const {return SnowdriftSpawnTimer;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    float GetEnemySpawnTimer() const{return EnemySpawnTimer;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetMaxSnowdrift() const {return MaxSnowdrifts;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    int32 GetMaxEnemies() const {return MaxEnemies;}
    UFUNCTION(BlueprintCallable, BlueprintPure)
    EGamePhase GetGamePhase() const {return GamePhase;}

    //Return max spawned actor count by type
    int32 GetMaxSpawnCountByType(ESpawnerType Type);
};
