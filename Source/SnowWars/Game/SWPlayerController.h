// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SWPlayerController.generated.h"

class APawn_Base;
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPossessCode, APawn*, Pawn);
/**
 * 
 */
UCLASS()
class SNOWWARS_API ASWPlayerController : public APlayerController
{
	GENERATED_BODY()

    UPROPERTY(BlueprintAssignable)
    FOnPossessCode OnPossessCode;
    
    UFUNCTION()
    virtual void OnPossess(APawn* InPawn) override;
    
    UFUNCTION()
    virtual void OnUnPossess() override;

public:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    bool bPlayerAlive = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    APawn_Base* CurrentPawn = nullptr;
    UFUNCTION(BlueprintCallable)
    void TryToRespawn();
};
