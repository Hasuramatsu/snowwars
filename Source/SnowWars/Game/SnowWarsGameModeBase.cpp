// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnowWarsGameModeBase.h"

#include "SWGameState.h"
#include "SWPlayerState.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "SnowWars/Pawn/Pawn_Base.h"



void ASnowWarsGameModeBase::BeginPlay()
{
    Super::BeginPlay();
    bGameEnded = false;
    bool bComplete = false;
    if(SnowdriftSpawners.IsValidIndex(0))
    {
        while(!bComplete)
        {
            for(const auto& It : SnowdriftSpawners)
            {
                It->SpawnNewActor();
                if(GetNumActorsOfType(It->GetType()) >= SnowdriftsStartCount)
                {
                    bComplete = true;
                    break;
                }
            
            }
        }
        //Start snowdrift spawn
        SnowdriftsTimeDel.BindUFunction(this, FName("ActivateSpawner"), SnowdriftSpawners);
        GetWorld()->GetTimerManager().SetTimer(SnowdriftTimerHandler, SnowdriftsTimeDel, GetGameState<ASWGameState>()->GetSnowdriftSpawnTimer(), true);
    }
    bComplete = false;
    if(EnemySpawners.IsValidIndex(0))
    {
        while(!bComplete)
        {
            for(const auto& It : EnemySpawners)
            {
                It->SpawnNewActor();
                if(GetNumActorsOfType(It->GetType()) >= EnemyStartCount)
                {
                    bComplete = true;
                    break;
                }
            
            }
        }
        //Start enemy spawn
        EnemiesTimeDel.BindUFunction(this, FName("ActivateSpawner"), EnemySpawners);
        GetWorld()->GetTimerManager().SetTimer(EnemyTimerHandler, EnemiesTimeDel, GetGameState<ASWGameState>()->GetEnemySpawnTimer(), true);
    }
}

void ASnowWarsGameModeBase::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    const bool bCanIn =  GetGameState<ASWGameState>()->GetGamePhase() == EGamePhase::FinalPhase;
    if(bCanIn)
    {
        FinalPhaseTimer -= DeltaSeconds;
        if(FinalPhaseTimer < 0)
        {
            EndGame();
        }
    }
}

void ASnowWarsGameModeBase::StartFinalPhase()
{
    //Start final phase
    GetGameState<ASWGameState>()->SetGamePhase(EGamePhase::FinalPhase);

    //Increase melting damage and frequency
    MakeMeltHarder();

    OnFinalStart.Broadcast();
    //When timer end, check logic for win game
   // GetWorld()->GetTimerManager().SetTimer(FinalPhaseTimeHandler, this, &ASnowWarsGameModeBase::EndGame, FinalPhaseDuration, false);
}

void ASnowWarsGameModeBase::EndGame()
{
    OnGameEnded.Broadcast();
    bGameEnded = true;
    //On timer end check player alive or not
    //If alive use win logic
    APawn_Base* MyPawn = Cast<APawn_Base>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
    if(MyPawn)
    {
        if(MyPawn->IsAlive())
        {
            WinGame();
        }
        else
        {
            LoseGame();
        } 
    }
    else
    {
        LoseGame();
    }  
}

void ASnowWarsGameModeBase::AddSpawner(ASpawner* Spawner)
{
    //Add spawned actor to matching type array
    switch (Spawner->GetType())
    {
        case ESpawnerType::EnemySpawner:
            EnemySpawners.Add(Spawner);
            break;
        case ESpawnerType::SnowdriftSpawner:
            SnowdriftSpawners.Add(Spawner);
            break;
        case ESpawnerType::None:
            UE_LOG(LogTemp, Warning, TEXT("ASnowWarsGameModeBase::AddSpawner -- Trying to add NONE spawner."));
            break;
        default:
            UE_LOG(LogTemp, Warning, TEXT("ASnowWarsGameModeBase::AddSpawner -- Undefined spawner class."))
            break;
    }
}

void ASnowWarsGameModeBase::TrackSpawnedActor(AActor* SpawnedActor, ESpawnerType Type)
{
    ASnowdrift* SpawnedSnowdrift = Cast<ASnowdrift>(SpawnedActor);
    AEnemy_Base* SpawnedEnemy = Cast<AEnemy_Base>(SpawnedActor);
    switch (Type)
    {
    case ESpawnerType::SnowdriftSpawner:
       
        if(SpawnedSnowdrift)
        {
            Snowdrifts.Add(SpawnedSnowdrift);
            //Remove track on actor destroy
            SpawnedSnowdrift->OnDestroyed.AddDynamic(this, &ASnowWarsGameModeBase::RemoveTrack);
        }
        break;
    case ESpawnerType::EnemySpawner:
        
        if(SpawnedEnemy)
        {
            Enemies.Add(SpawnedEnemy);
            UpdateStats(SpawnedEnemy);
            //Remove track on actor destroy
            SpawnedEnemy->OnDefeat.AddDynamic(this, &ASnowWarsGameModeBase::RemoveTrack);
        }
        break;
    default:
        UE_LOG(LogTemp, Warning, TEXT("ASnowWarsGameModeBase::GetNumActorsOfType -- undefined type."));
        break;
    }
}

void ASnowWarsGameModeBase::RemoveTrack(AActor* Actor)
{
    ASnowdrift* SnowdriftToRemove = Cast<ASnowdrift>(Actor);
    if(SnowdriftToRemove)
    {
        //Find and remove actor from array
        const int32 It = Snowdrifts.Find(SnowdriftToRemove);
        Snowdrifts.RemoveAt(It);
        return;
    }
    AEnemy_Base* EnemyToRemove = Cast<AEnemy_Base>(Actor);
    if(EnemyToRemove)
    {
        //Find and remove actor from array
        const auto It = Enemies.Find(EnemyToRemove);
        Enemies.RemoveAt(It);
    }
}

void ASnowWarsGameModeBase::ActivateSpawner(TArray<ASpawner*> Array)
{
    //Check we have spawner
    if(Array.IsValidIndex(0))
    {
        //Get current and max count of spawned actors of type
        const int32 CurrentCount = GetNumActorsOfType(Array[0]->GetType());
        const int32 MaxCount = GetGameState<ASWGameState>()->GetMaxSpawnCountByType(Array[0]->GetType());
        const bool Result = CurrentCount < MaxCount;
        if(Result)
        {
            //Chose random spawner to spawn actor
            const int8 Tmp = FMath::RandRange(0, Array.Num() - 1);
           const bool bIsSpawned = Array[Tmp]->SpawnNewActor();
            if(!bIsSpawned)
            {
                Array[Tmp]->SpawnNewActor(true);
            }
            
        }
    }
    
    
}

int32 ASnowWarsGameModeBase::GetNumActorsOfType(ESpawnerType Type)
{

    //Return count actors of chosen spawner type 
    int32 Tmp = 0;
    switch (Type)
    {
    case ESpawnerType::SnowdriftSpawner:
        Tmp = static_cast<int32>(Snowdrifts.Num());
        break;
    case ESpawnerType::EnemySpawner:
        Tmp = static_cast<int32>(Enemies.Num());
        break;
    default:
        UE_LOG(LogTemp, Warning, TEXT("ASWGameState::GetMaxSpawnCountByType -- undefined type."));
        break;
    }
    return Tmp;
}

void ASnowWarsGameModeBase::RiseDifficulty()
{
    //Increasing stats of all alive enemies
    for(auto It : Enemies)
    {
        It->IncreaseChasingSpeedByValue(DifficultySpeedUp);
        It->IncreaseDamageByValue(DifficultyDamageUp);
    }
    GetGameState<ASWGameState>()->EnemySpawnTimer -= DifficultyEnemySpawnTimerCoef;
    GetGameState<ASWGameState>()->SnowdriftSpawnTimer += DifficultySnowdriftSpawnTimerCoef;
}

void ASnowWarsGameModeBase::UpdateStats(AEnemy_Base* Enemy)
{
    float* ChasingSpeed = Enemy->GetActualChasingSpeed();
    float* Damage = Enemy->GetActualDamage();
    *ChasingSpeed += DifficultySpeedUp * GetGameState<ASWGameState>()->GetDifficultyLevel();
    *Damage += DifficultyDamageUp * GetGameState<ASWGameState>()->GetDifficultyLevel();
}

void ASnowWarsGameModeBase::EnableMelting()
{
    GetWorld()->GetTimerManager().SetTimer(MeltingTimerHandler, this, &ASnowWarsGameModeBase::Melt, MeltTimer, true);
}

void ASnowWarsGameModeBase::MakeMeltHarder()
{
    MeltDamage = HardMeltDamage;
    MeltTimer = HardMeltTimer;
}

void ASnowWarsGameModeBase::Melt() const
{
    APawn_Base* MyPawn = Cast<APawn_Base>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
    if(MyPawn)
    {
        UGameplayStatics::ApplyDamage(MyPawn, MeltDamage, MyPawn->GetController(), MyPawn, UDamageType::StaticClass());
    }
}

void ASnowWarsGameModeBase::DelayMelting()
{
    //Stop melting timer and reactivate it after delay
    GetWorld()->GetTimerManager().ClearTimer(MeltingTimerHandler);
    GetWorld()->GetTimerManager().SetTimer(DelayMeltingTimerHandler, this, &ASnowWarsGameModeBase::EnableMelting, MeltDelayTimer, false);
}

void ASnowWarsGameModeBase::AttemptToRespawn(APlayerController* Controller)
{
    ASWPlayerState* MyPlayerState = UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPlayerState<ASWPlayerState>();
    if(MyPlayerState)
    {
        if(MyPlayerState->GetCurrentLives() > 1)
        {
            MyPlayerState->Death(); // Decrement current Lives count
            TArray<AActor*> PlayerSpawns;
            UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerSpawns); //Found spawn point
            if(PlayerSpawns.IsValidIndex(0)) // Check spawn point is valid
            {
                Controller->bAutoManageActiveCameraTarget = true;
                RestartPlayerAtPlayerStart(Controller, PlayerSpawns[0]);                      
            }
        }
        else
        {
            EndGame();
        }
    }         
    else
    {
        EndGame();
    }
}


void ASnowWarsGameModeBase::LoseGame_Implementation()
{
    //GetWorld()->GetTimerManager().ClearTimer(FinalPhaseTimeHandler);
    FinalPhaseTimer = 0.f;
    //BP override
}

void ASnowWarsGameModeBase::WinGame_Implementation()
{
    GetWorld()->GetTimerManager().ClearTimer(EnemyTimerHandler);
    while(Enemies.IsValidIndex(0))
    {
        Enemies[0]->Defeat();
    }
    
    //BP override
}

