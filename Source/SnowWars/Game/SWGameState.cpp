// Fill out your copyright notice in the Description page of Project Settings.


#include "SWGameState.h"


#include "SnowWarsGameModeBase.h"
#include "Kismet/GameplayStatics.h"

void ASWGameState::UpdateDifficultyPoints(int32 Score)
{
    DifficultyPoints = StartDifficultyPoints + static_cast<int32>(Score / DivideCoef);
    UpdateDifficulty();
    OnDifficultyPointChange.Broadcast(DifficultyPoints);
}

void ASWGameState::UpdateDifficulty()
{
    ASnowWarsGameModeBase* MyGameMode = Cast<ASnowWarsGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
    if(MyGameMode)
    {
        const FVector2D InputRange = FVector2D(StartDifficultyPoints, MaxDifficultyPoints);
        const FVector2D OutputRange = FVector2D(1.f, MaxDifficulty);
        const int OldDifficulty = DifficultyLevel;
        DifficultyLevel = static_cast<int32>(FMath::GetMappedRangeValueClamped(InputRange, OutputRange, DifficultyPoints));
        if(OldDifficulty != DifficultyLevel)
        {
            MyGameMode->RiseDifficulty();
            
        }
        if(DifficultyLevel == 4)
        {
            GamePhase = EGamePhase::HardPhase;
            MyGameMode->EnableMelting();
        }
        else if(DifficultyLevel == 5 && GamePhase != EGamePhase::FinalPhase)
        {
            MyGameMode->StartFinalPhase();
        }
    }
}

void ASWGameState::SetGamePhase(EGamePhase Phase)
{
    GamePhase = Phase;
}

int32 ASWGameState::GetMaxSpawnCountByType(ESpawnerType Type)
{
    int32 Tmp = 0;
    switch (Type)
    {
    case ESpawnerType::SnowdriftSpawner:
        Tmp = MaxSnowdrifts;
        break;
    case ESpawnerType::EnemySpawner:
        Tmp = MaxEnemies;
        break;
    default:
        UE_LOG(LogTemp, Warning, TEXT("ASWGameState::GetMaxSpawnCountByType -- undefined type."));
        break;
    }
    return Tmp;
}
