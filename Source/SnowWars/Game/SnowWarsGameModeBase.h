// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnowWars/Objects/Snowdrift.h"
#include "SnowWars/Pawn/Enemy_Base.h"
#include "SnowWars/UtilityActors/Spawner.h"

#include "SnowWarsGameModeBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFinalStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameEnded);
/**
 * 
 */
UCLASS()
class SNOWWARS_API ASnowWarsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	//Starting final phase of the game
	void StartFinalPhase();

	//Final phase duration
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FinalPhase")
	float FinalPhaseDuration = 15.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FinalPhase")
	bool bGameEnded = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Difficulty")
	float DifficultySpeedUp = 50.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Difficulty")
	float DifficultyDamageUp = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Difficulty")
	float DifficultyEnemySpawnTimerCoef = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Difficulty")
	float DifficultySnowdriftSpawnTimerCoef = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Melting")
	float MeltTimer = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Melting")
	float MeltDelayTimer = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Melting")
	float MeltDamage = 2.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Melting")
	float HardMeltTimer = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Melting")
	float HardMeltDamage = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BeginPlay")
	int32 EnemyStartCount = 12;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BeginPlay")
	int32 SnowdriftsStartCount = 16;

private:
	//FTimerHandle FinalPhaseTimeHandler;
	FTimerHandle SnowdriftTimerHandler;
	FTimerHandle EnemyTimerHandler;
	FTimerHandle MeltingTimerHandler;
	FTimerHandle DelayMeltingTimerHandler;
	FTimerDelegate SnowdriftsTimeDel;
	FTimerDelegate EnemiesTimeDel;

	//Tick timer
	float FinalPhaseTimer = FinalPhaseDuration;
	//Pointers on all spawners in game
	UPROPERTY()
	TArray<ASpawner*> SnowdriftSpawners;
	UPROPERTY()
	TArray<ASpawner*> EnemySpawners;

	//All actors spawned by spawners
	UPROPERTY()
	TArray<ASnowdrift*> Snowdrifts;
	UPROPERTY()
	TArray<AEnemy_Base*> Enemies;

	//Delegates
	UPROPERTY(BlueprintAssignable)
	FOnFinalStart OnFinalStart;
	UPROPERTY(BlueprintAssignable)
	FOnGameEnded OnGameEnded;

protected:
	//Called on start game
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;

	
	//Called on final phase timer end
	//Check for win or lose
	UFUNCTION()
	void EndGame();

	//Blueprint implementation of win game
	UFUNCTION(BlueprintNativeEvent)
	void WinGame();

	//Blueprint implementation of lose game
	UFUNCTION(BlueprintNativeEvent)
    void LoseGame();

	//Add spawner to array. Called by spawner on BeginPlay
	UFUNCTION()
	void AddSpawner(ASpawner* Spawner);

	//Add spawned actor to array
	void TrackSpawnedActor(AActor* SpawnedActor, ESpawnerType Type);
	//Remove destroyed actor
	UFUNCTION()
	void RemoveTrack(AActor* Actor);

	//Try Activate Spawner
	UFUNCTION()
	void ActivateSpawner(TArray<ASpawner*> Array);


	//Getters
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetNumActorsOfType(ESpawnerType Type);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetCurrentFinalPhaseTimer() const {return FinalPhaseTimer;}
	UFUNCTION(BlueprintCallable, BlueprintPure)
    float GetFinalPhaseTimer() const {return FinalPhaseDuration;}
	

	//Update stats of all spawned elements
	UFUNCTION(BlueprintCallable)
	void RiseDifficulty();

	//Update stats of new spawn elements
	UFUNCTION()
	void UpdateStats(AEnemy_Base* Enemy);

	//Enable player dot
	UFUNCTION()
	void EnableMelting();

	//Increase melting damage and frequency
	UFUNCTION()
	void MakeMeltHarder();

	//Apply damage to player
	UFUNCTION()
	void Melt() const;

	//Dealay melting
	UFUNCTION()
	void DelayMelting();

	UFUNCTION()
	void AttemptToRespawn(APlayerController* Controller);
};

