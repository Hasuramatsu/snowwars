// Fill out your copyright notice in the Description page of Project Settings.


#include "SWPlayerState.h"


#include "SWGameState.h"
#include "Kismet/GameplayStatics.h"

void ASWPlayerState::AddScore()
{
    //Increase current Score, then update difficulty
    CurrentScore += ScoreForKill;
    ASWGameState* MyGameState = Cast<ASWGameState>(UGameplayStatics::GetGameState(GetWorld()));
    if(MyGameState)
    {
        MyGameState->UpdateDifficultyPoints(CurrentScore);
    }
}

void ASWPlayerState::Death()
{
    CurrentLives--;
}
