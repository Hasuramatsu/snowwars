// Fill out your copyright notice in the Description page of Project Settings.


#include "SWAIController.h"



#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"

#include "SnowWars/Pawn/Pawn_Base.h"


ASWAIController::ASWAIController()
{
    //Create components
    AIPerceptionComponent = GetAIPerceptionComponent();
    AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerceptionComponent"));
    AISightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));

    //Set to detect all types
    AISightConfig->DetectionByAffiliation.bDetectEnemies = true;
    AISightConfig->DetectionByAffiliation.bDetectFriendlies = true;
    AISightConfig->DetectionByAffiliation.bDetectNeutrals = true;
    //Set sense
    AIPerceptionComponent->ConfigureSense(*AISightConfig);

    //
    AIPerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &ASWAIController::UpdatePlayer);
    
}

void ASWAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

}

void ASWAIController::BeginPlay()
{
    Super::BeginPlay();

    if(AIBehaviorTree)
    {
        RunBehaviorTree(AIBehaviorTree);
        AIBlackboard = UAIBlueprintHelperLibrary::GetBlackboard(this);
    }
    
}

void ASWAIController::UpdatePlayer(AActor* Actor, FAIStimulus Stimulus)
{
    if(Actor)
    {
        APawn_Base* MyPawn = Cast<APawn_Base>(Actor);
        if(MyPawn == UGameplayStatics::GetPlayerPawn(GetWorld(), 0))
        {
            if(MyPawn != TargetActor)
            {
                TargetActor = MyPawn;
                if(Blackboard)
                {
                    Blackboard->SetValueAsObject("TargetPlayer", TargetActor);
                }               
            }
        }
    }
}

