// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AISenseConfig_Sight.h"
#include "SnowWars/Pawn/Pawn_Base.h"


#include "SWAIController.generated.h"

/**
 * 
 */
UCLASS()
class SNOWWARS_API ASWAIController : public AAIController
{
	GENERATED_BODY()

	
public:
	ASWAIController();

	//Behavior tree
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "BehaviorTree" )
	UBehaviorTree* AIBehaviorTree;

	virtual void OnPossess(APawn* InPawn) override;
	

protected:
	virtual void BeginPlay() override;

private:
	//Preception component
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	UAIPerceptionComponent* AIPerceptionComponent = nullptr;

	//Sight sense for perception component
	UPROPERTY()
	UAISenseConfig_Sight* AISightConfig = nullptr;

	//Current found target
	UPROPERTY()
	APawn_Base* TargetActor = nullptr;

	UPROPERTY()
	UBlackboardComponent* AIBlackboard = nullptr;

	//Update blackboard TargetPlayer on find target
	UFUNCTION()
	void UpdatePlayer(AActor* Actor, FAIStimulus Stimulus);

};


