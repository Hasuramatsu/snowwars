// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SWPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class SNOWWARS_API ASWPlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	//Score getting on kill
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int32 ScoreForKill = 5;

	//Max character lives
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lives")
	int32 MaxLives = 3;

private:
	//CurrentScore
	UPROPERTY(VisibleAnywhere, Category = "Score")
	int32 CurrentScore = 0;

	//CurrentLives
	UPROPERTY(VisibleAnywhere, Category = "Lives")
	int32 CurrentLives = MaxLives;

public:

	//Increase score by const var
	UFUNCTION(BlueprintCallable)
	void AddScore();

	//Decrement current Lives
	UFUNCTION(BlueprintCallable)
    void Death();

	//Getters
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentScore() const {return CurrentScore;};
	UFUNCTION(BlueprintCallable)
    int32 GetCurrentLives() const {return CurrentLives;};
};
