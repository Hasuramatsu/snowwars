// Fill out your copyright notice in the Description page of Project Settings.


#include "SWPlayerController.h"


#include "SnowWarsGameModeBase.h"
#include "Kismet/GameplayStatics.h"

void ASWPlayerController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    bPlayerAlive = true;

    OnPossessCode.Broadcast(InPawn);
}

void ASWPlayerController::OnUnPossess()
{
    Super::OnUnPossess();

    bPlayerAlive = false;
   
}

void ASWPlayerController::TryToRespawn()
{
    ASnowWarsGameModeBase* MyGameMode = Cast<ASnowWarsGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
    if(MyGameMode)
    {
       MyGameMode->AttemptToRespawn(this); 
    }
}
